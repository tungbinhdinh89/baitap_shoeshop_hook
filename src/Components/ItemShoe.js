import React, { useState } from 'react';
import { Card } from 'antd';
const { Meta } = Card;

export default function ItemShoe({
  dataShoe,
  handleDetailShoe,
  addItemToCart,
}) {
  let { image, name, price, quantity } = dataShoe;
  return (
    <div className="text-left pt-4">
      <Card
        hoverable
        size={'small'}
        style={{ width: 240 }}
        cover={<img alt="example" src={image} />}>
        <Meta title={`Name: ${name}`} />
        <Meta title={`Price: ${price}`} />
        <Meta title={`Quantity: ${quantity}`} />

        <button
          className="btn btn-primary rounded mt-2 mr-2"
          onClick={() => {
            handleDetailShoe(dataShoe);
          }}>
          Detail
        </button>
        <button
          className="btn btn-success rounded mt-2"
          onClick={() => {
            addItemToCart(dataShoe);
          }}>
          Add to Cart
        </button>
      </Card>
    </div>
  );
}
