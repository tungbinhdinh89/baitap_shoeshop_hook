import React from 'react';
import ItemShoe from '../Components/ItemShoe';

export default function ListShoe({
  listShoe,
  handleDetailShoe,
  addItemToCart,
}) {
  let renderListShoe = () => {
    return listShoe.map((shoe, index) => {
      return (
        <ItemShoe
          dataShoe={shoe}
          key={index}
          handleDetailShoe={handleDetailShoe}
          addItemToCart={addItemToCart}
        />
      );
    });
  };
  return (
    <div className="grid grid-cols-3 gap-0 mx-auto">{renderListShoe()}</div>
  );
}
