import React, { useState } from 'react';
import ListShoe from './ListShoe';
import CartShoe from './CartShoe';
import DetailShoe from './DetailShoe';
import { shoeArr } from './dataShoe';

export default function ShoeShop() {
  let [listShoe, setListShoe] = useState(shoeArr);
  let [shoeDetai, setShoeDetail] = useState(shoeArr[0]);
  let [cartItem, setCartItem] = useState([]);
  console.log('🚀 ~ Tung:', cartItem);

  // change detail shoe
  const handleDetailShoe = (shoe) => {
    setShoeDetail(shoe);
  };

  // add shoe to cart
  const addItemToCart = (shoe) => {
    let cloneCart = [...cartItem];
    let index = cloneCart.findIndex((item) => {
      return shoe.id === item.id;
    });
    if (index === -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
      setCartItem(cloneCart);
    } else {
      cloneCart[index].quantity++;
      setCartItem(cloneCart);
    }
  };

  // change quantity
  const changeQuantity = (id, number) => {
    let cloneCart = [...cartItem];
    let index = cloneCart.findIndex((item) => {
      return item.id === id;
    });
    cloneCart[index].quantity = cloneCart[index].quantity + number;
    cloneCart[index].quantity === 0 && cloneCart.splice(index, 1);
    setCartItem(cloneCart);
  };

  // delete cart item
  const handleDelete = (id) => {
    let cloneCart = [...cartItem];
    let index = cloneCart.findIndex((item) => {
      return id === item.id;
    });
    cloneCart.splice(index, 1);
    setCartItem(cloneCart);
  };
  return (
    <div>
      <div className="row">
        <div className="col-6">
          <CartShoe
            cartItem={cartItem}
            changeQuantity={changeQuantity}
            handleDelete={handleDelete}
          />
        </div>
        <div className="col-6">
          <ListShoe
            listShoe={listShoe}
            handleDetailShoe={handleDetailShoe}
            addItemToCart={addItemToCart}
          />
        </div>
      </div>
      <DetailShoe shoeDetai={shoeDetai} />
    </div>
  );
}
