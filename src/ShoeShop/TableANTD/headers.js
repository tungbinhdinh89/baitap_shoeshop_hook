export let headers = [
  {
    title: 'ID',
    dataIndex: '',
    key: '',
  },
  {
    title: 'Name',
    dataIndex: '',
    key: '',
  },
  {
    title: 'Price',
    dataIndex: '',
    key: '',
  },
  {
    title: 'Stock',
    dataIndex: '',
    key: '',
  },
  {
    title: 'Quantity',
    dataIndex: '',
    key: '',
  },
  {
    title: 'Action',
    dataIndex: '',
    key: '',
  },
];
