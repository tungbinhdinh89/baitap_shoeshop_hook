import React from 'react';
import { Space, Table, Tag } from 'antd';
import { headers } from './TableANTD/headers';

export default function CartShoe({ cartItem, changeQuantity, handleDelete }) {
  const renderCartShoe = () => {
    return cartItem.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img style={{ width: 80 }} src={item.image} />
          </td>
          <td>
            <button
              onClick={() => {
                changeQuantity(item.id, -1);
              }}
              className="btn btn-danger">
              -
            </button>
            <strong className="mx-3">{item.quantity}</strong>
            <button
              className="btn btn-primary"
              onClick={() => {
                changeQuantity(item.id, 1);
              }}>
              +
            </button>
          </td>
          <td>
            <button
              className="btn border-danger text-danger"
              onClick={() => {
                handleDelete(item.id);
              }}>
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Image</th>
            <th>Quantity</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{renderCartShoe()}</tbody>
      </table>
    </div>
  );
}
