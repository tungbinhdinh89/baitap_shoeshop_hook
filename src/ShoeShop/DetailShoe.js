import React, { useState } from 'react';
import { shoeArr } from './dataShoe';
import { Card } from 'antd';
const { Meta } = Card;

export default function ({ shoeDetai }) {
  let { name, price, description, shortDescription, quantity, image } =
    shoeDetai;
  return (
    <div className="text-left pt-4">
      <Card
        hoverable
        size={'small'}
        style={{ width: 240 }}
        cover={<img alt="example" src={image} />}>
        <Meta title={`Name: ${name}`} />
        <Meta title={`Price: ${price}`} />
        <Meta title={`Quantity: ${quantity}`} />
        <Meta title={`Description: ${description}`} />
        <Meta title={`Short Description: ${shortDescription}`} />
      </Card>
    </div>
  );
}
